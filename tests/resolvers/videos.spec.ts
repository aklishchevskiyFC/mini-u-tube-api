import { should } from 'chai';
import * as fs from 'fs';
import { afterEach, beforeEach, describe, it } from 'mocha';
import * as path from 'path';
import { assert, createSandbox, SinonStub } from 'sinon';
import * as config from '../../src/config';
import { videos } from '../../src/graphql/resolvers/Query/videos';

should();

describe(
  'videos endpoint', () => {
    let sandBox;

    beforeEach(() => {
      sandBox = createSandbox();

      sandBox.stub(fs, 'readdirSync').returns(['1234-1234-1234-1234.test_meta']);
      sandBox.stub(fs, 'readFileSync').returns(JSON.stringify({
        encoding: '7bit',
        filename: 'test file name',
        mimetype: 'video/mp4'
      }));

      sandBox.stub(config, 'FILES_FOLDER').value('test_folder_full');
      sandBox.stub(config, 'FILES_FOLDER_NAME').value('test_folder');
      sandBox.stub(config, 'META_EXTENSION').value('.test_meta');
    });

    afterEach(() => {
      sandBox.restore();
      sandBox.reset();
    });

    it('should call folder reading', async () => {
      await videos();

      assert.calledWith(
        fs.readdirSync as SinonStub,
        'test_folder_full'
      );
      assert.calledWith(
        fs.readFileSync as SinonStub,
        path.join('test_folder_full', '1234-1234-1234-1234.test_meta'),
        'utf8'
      );
    });

    it('should return list of videos', async () => {
      const res = await videos();

      res.should.be.deep.equal([
        {
          id: '1234-1234-1234-1234',
          url: '/test_folder/1234-1234-1234-1234',
          encoding: '7bit',
          filename: 'test file name',
          mimetype: 'video/mp4'
        }
      ]);
    });
  }
);