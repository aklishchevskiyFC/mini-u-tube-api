import { should } from 'chai';
import * as fs from 'fs';
import { afterEach, beforeEach, describe, it } from 'mocha';
import { assert, createSandbox, match, SinonStub } from 'sinon';
import { Readable } from 'stream';
import * as uuid from 'uuid';
import * as config from '../../src/config';
import { FILES_FOLDER, META_EXTENSION } from '../../src/config';
import { upload } from '../../src/graphql/resolvers/Mutation/upload';

should();

describe(
  'upload endpoint',
  () => {
    let sandBox;

    beforeEach(() => {
      sandBox = createSandbox();

      sandBox.stub(fs, 'open');
      sandBox.stub(fs, 'write');
      sandBox.stub(fs, 'close');

      sandBox.stub(fs, 'existsSync').returns(false);
      sandBox.stub(fs, 'mkdirSync');
      sandBox.stub(fs, 'createWriteStream').returns(new fs.WriteStream('dummy' as any));
      sandBox.stub(fs, 'writeFileSync');

      sandBox.stub(uuid, 'v4').returns('really_random_uuid');

      sandBox.stub(config, 'FILES_FOLDER').value('test_folder_full');
      sandBox.stub(config, 'FILES_FOLDER_NAME').value('test_folder');
      sandBox.stub(config, 'META_EXTENSION').value('.test_meta');
    });

    afterEach(() => {
      sandBox.restore();
      sandBox.reset();
    });

    it('should call video saving', async () => {
      await upload(
        undefined,
        {
          file: Promise.resolve(
            {
              filename: 'filename test',
              mimetype: 'mimetype test',
              encoding: 'encoding test',
              stream: Readable.from('file content test') as fs.ReadStream
            }
          )
        }
      );

      assert.calledWith(fs.existsSync as SinonStub, 'test_folder_full');
      assert.calledWith(fs.mkdirSync as SinonStub, 'test_folder_full');
      assert.calledWith(fs.createWriteStream as SinonStub, match(/test_folder_full\/.*/));
      assert.calledWith(fs.writeFileSync as SinonStub,
        match(/test_folder_full\/.*\.test_meta/),
        JSON.stringify({
          encoding: 'encoding test',
          filename: 'filename test',
          mimetype: 'mimetype test'
        }));
    });

    it('should return video', async () => {
      const res = await upload(
        undefined,
        {
          file: Promise.resolve(
            {
              filename: 'filename test',
              mimetype: 'mimetype test',
              encoding: 'encoding test',
              stream: Readable.from('file content test') as fs.ReadStream
            }
          )
        }
      );

      res.should.to.have.all.keys(
        'id',
        'encoding',
        'filename',
        'mimetype',
        'url'
      );
      res.should.to.own.include({
        filename: 'filename test',
        mimetype: 'mimetype test',
        encoding: 'encoding test'
      });

      res.id.should.to.be.a('string');
      res.url.should.match(
        /\/test_folder\/.*/
      );
    });
  }
);