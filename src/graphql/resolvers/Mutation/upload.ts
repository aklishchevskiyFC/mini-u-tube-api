import { IUpload } from '../../../abstract/IUpload';
import { IVideo } from '../../../abstract/IVideo';
import * as VideoService from '../../../services/VideoService';

export const upload = async (
  node: undefined,
  { file }: { file: Promise<IUpload> }
): Promise<IVideo> => VideoService.addVideo(await file);
