import { IVideo } from '../../../abstract/IVideo';
import * as VideoService from '../../../services/VideoService';

export const videos = async (): Promise<Array<IVideo>> => VideoService.getVideos();
