import { config } from 'dotenv';
import * as path from 'path';

config();

export const PORT = process.env.PORT;
export const FILES_FOLDER_NAME = process.env.FILES_FOLDER;
export const FILES_FOLDER = path.join(process.cwd(), FILES_FOLDER_NAME);
export const META_EXTENSION = '.meta';