export interface IVideo {
  id: string;
  filename: string;
  mimetype: string;
  encoding: string;
  url: string;
  created: string;
}