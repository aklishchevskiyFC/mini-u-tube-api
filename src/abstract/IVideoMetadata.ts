export interface IVideoMetadata {
  encoding: string;
  filename: string;
  mimetype: string;
  created: string;
}