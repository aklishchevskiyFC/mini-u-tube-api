import { ReadStream } from 'fs';

export interface IUpload {
  filename: string;
  mimetype: string;
  encoding: string;
  stream?: ReadStream;
}
