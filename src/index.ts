import * as cors from '@koa/cors';
import { ApolloServer } from 'apollo-server-koa';
import * as Koa from 'koa';
import * as bodyParser from 'koa-bodyparser';
import * as mount from 'koa-mount';
import * as Router from 'koa-router';
import * as serve from 'koa-static';
import { FILES_FOLDER_NAME, PORT } from './config';
import schema from './graphql';

const app = new Koa();
const router = new Router();

// koaBody is needed just for POST.
app.use(bodyParser());
app.use(cors());
app.use(mount(`/${FILES_FOLDER_NAME}`, serve(FILES_FOLDER_NAME, {})));

const server = new ApolloServer({
  schema
});

server.applyMiddleware({ app });

app.use(router.routes());
app.use(router.allowedMethods());
app.listen(PORT);