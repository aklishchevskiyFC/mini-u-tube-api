import * as fs from 'fs';
import * as path from 'path';
import { v4 } from 'uuid';
import { IUpload } from '../../abstract/IUpload';
import { IVideo } from '../../abstract/IVideo';
import { FILES_FOLDER, META_EXTENSION } from '../../config';
import { makeVideoUrl } from './makeVideoUrl';

export const addVideo = async ({ encoding, filename, mimetype, stream }: IUpload): Promise<IVideo> => {
  const id = v4();

  if (!fs.existsSync(FILES_FOLDER)) {
    fs.mkdirSync(FILES_FOLDER);
  }

  const writeStream = fs.createWriteStream(path.join(FILES_FOLDER, id));

  await new Promise((resolve, reject) => {
    stream.pipe(writeStream);
    stream.on('end', resolve);
    stream.on('error', reject);
  });

  const created = `${Date.now()}`;

  fs.writeFileSync(
    path.join(FILES_FOLDER, `${id}${META_EXTENSION}`),
    JSON.stringify({ encoding, filename, mimetype, created }),
    { encoding: 'utf8' }
  );

  return {
    id,
    encoding,
    filename,
    mimetype,
    created,
    url: makeVideoUrl(id)
  };
};