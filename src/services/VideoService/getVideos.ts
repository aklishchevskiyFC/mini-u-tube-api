import * as fs from 'fs';
import * as path from 'path';
import { IVideo } from '../../abstract/IVideo';
import { IVideoMetadata } from '../../abstract/IVideoMetadata';
import { FILES_FOLDER, META_EXTENSION } from '../../config';
import { makeVideoUrl } from './makeVideoUrl';

export const getVideos = async (): Promise<Array<IVideo>> => {
  const fileNames = fs.readdirSync(FILES_FOLDER);

  return (fileNames || []).map(fileNameMeta => {
    if (!fileNameMeta.endsWith(META_EXTENSION)) {
      return;
    }

    const id = fileNameMeta.replace(META_EXTENSION, '');

    const meta: IVideoMetadata = JSON.parse(
      fs.readFileSync(path.join(FILES_FOLDER, fileNameMeta), 'utf8')
    );

    return {
      ...meta,
      id,
      url: makeVideoUrl(id)
    };
  }).filter(m => !!m);
};