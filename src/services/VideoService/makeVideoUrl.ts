import { FILES_FOLDER_NAME } from '../../config';

export const makeVideoUrl = (id: string): string => `/${FILES_FOLDER_NAME}/${id}`;